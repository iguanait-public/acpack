#!/bin/bash

if [ ! -f "/usr/bin/wget" ]; then
    DEBIAN_FRONTEND=noninteractive
    apt-get update
    apt-get install wget -y
fi

if [ -z "/var/lib/postgresql/data" ]; then
    SQLFILE=acdb.sql.gz

    wget -O /docker-entrypoint-initdb.d/$SQLFILE --http-user=public --http-password=access https://acb.axonstream.com/protected-download/$SQLFILE
    chmod 0666 /docker-entrypoint-initdb.d/$SQLFILE
fi

/docker-entrypoint.sh postgres