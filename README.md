To run the stack of access controll apps we can use multiple ways. 

1. Login to GitLab docker registry
   docker login registry.gitlab.com

   Use your GitLab user/pass
2. Clone the repo https://gitlab.com/iguanait-public/acpack
3. Edit docker-compose.yml and change ports mapping in order to prevent conflict with already used ports
4. Run docker apps using one of the following ways
   a) Docker compose up

   docker-compose -f docker-compose.yml up

   This will start all services one by one.
   NOTE: CTRL+C will stop all docker instances. If you need to run them again, you need to start each instance separately starting from postgresql, elastic, backend and frontend

   docker start ac-postgresql
   docker start ac-elasticsearch
   docker start ac-backend
   docker start ac-frontend

   b) Docker compose create

   docker-compose -f docker-compose.yml up

   This will create containers, but will not start them. You need to start the services manually

   docker start ac-postgresql
   docker start ac-elasticsearch
   docker start ac-backend
   docker start ac-frontend

   b) Docker compose start

   docker-compose -f docker-compose.yml start

   This will start all services one by one.